﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab1FrameWork
{
    public class Algorithm
    {
        readonly List<string> functions = new List<string>() { "sin", "cos", "tg", "abs", "exp", "pow", "sqrt" };
        readonly List<string> operators = new List<string>() { "*", "/", "+", "-" };
        readonly List<string> constants = new List<string>() { "E", "PI" };

        static Dictionary<string, Func<double, double, double>> operators_dict = new Dictionary<string, Func<double, double, double>>();
        static Dictionary<string, Func<double, double>> funcs_dict = new Dictionary<string, Func<double, double>>();

        public Algorithm()
        {
            operators_dict.Add("+", MathOperators.Add);
            operators_dict.Add("-", MathOperators.Sub);
            operators_dict.Add("*", MathOperators.Mult);

            funcs_dict.Add("sin", MathOperators.Sin);
            funcs_dict.Add("cos", MathOperators.Cos);
            funcs_dict.Add("tg", MathOperators.Tg);
            funcs_dict.Add("exp", MathOperators.Exp);
            funcs_dict.Add("abs", MathOperators.Abs);
        }
        public List<string> TokenizeString(string input)
        {
            List<string> tokens = new List<string>();
            var matches = Regex.Matches(input, @"(\(\-\d*\,?\d+\))|(\d*\,\d*|\w+)|[\(\)\+\*\-\/]");
            foreach (Match match in matches)
            {
                if (Regex.IsMatch(match.Value, @"\(\-\d*\,?\d+\)"))
                {
                    List<string> a = new List<string>(); 
                    var insidemathces = Regex.Matches(match.Value, @"(-\d*\,?\d+)|[\(\)]"); 
                    foreach (Match item in insidemathces) 
                        a.Add(item.Value); tokens.AddRange(a);
                }
                else
                    tokens.Add(match.Value);
            }
            return tokens;
            //return Regex.Matches(input, @"\b\w*\.?\w+\b|[\(\)\+\*\-\/]").Select(p=>p.Value).ToList();
        }
        public static bool IsDouble(string token)
        {
            double res;
            if (double.TryParse(token, out res))
                return true;
            return false;
        }

        public List<string> Parse_To_RPN(List<string> tokens)
        {
            List<string> output_queue = new List<string>();
            Stack<string> stack = new Stack<string>();

            foreach (var token in tokens)
            {
                if (IsDouble(token) || constants.Contains(token) || token == "x")
                    output_queue.Add(token);
                else if (functions.Contains(token) || token == "(")
                    stack.Push(token);
                else if (token == ")")
                {
                    while (stack.Count > 0 && stack.Peek() != "(")
                        output_queue.Add(stack.Pop());
                    if (stack.Count > 0)
                        stack.Pop();
                    else
                        throw new Exception("Wrong right parentheses in formula");
                }
                else if (operators.Contains(token))
                {
                    while (stack.Count > 0 &&
                           ((functions.Contains(stack.Peek())) || ((operators.Contains(stack.Peek())) && ((operators.IndexOf(token) / 2) >= (operators.IndexOf(stack.Peek()) / 2))) &&
                           stack.Peek() != "("))
                        output_queue.Add(stack.Pop());
                    stack.Push(token);
                }
                else
                    throw new Exception("Wrong function name");
            }
            while (stack.Count > 0)
                output_queue.Add(stack.Pop());
            return output_queue;
        }
        public void ShowStack(Stack<string> stack)
        {
            foreach (var i in stack)
                Console.Write(i + " ");
            Console.WriteLine();
        }
        public double Calculate_RPN(List<string> rpn)
        {
            Stack<string> stack = new Stack<string>();

            foreach (var token in rpn)
            {
                if (IsDouble(token))
                    stack.Push(token);
                else if (operators.Contains(token))
                {
                    double value_1 = Convert.ToDouble(stack.Pop());
                    double value_2 = Convert.ToDouble(stack.Pop());
                    if (token == "/")
                    {
                        if (value_1 == 0)
                            throw new Exception("Devide by 0");
                        else
                            stack.Push((value_2 / value_1).ToString());
                    }
                    else
                        stack.Push(operators_dict[token](value_2, value_1).ToString());
                }
                else if (functions.Contains(token))
                {
                    if (token == "pow")
                    {
                        double value_1 = Convert.ToDouble(stack.Pop());
                        double value_2 = Convert.ToDouble(stack.Pop());
                        if (value_1 == 0 && value_2 == 0)
                            throw new Exception("Zero pow zero");
                        else
                            stack.Push(Math.Pow(value_2, value_1).ToString());
                    }
                    else if (token == "sqrt")
                    {
                        double value = Convert.ToDouble(stack.Pop());
                        if (value < 0)
                            throw new Exception("Negative value under sqrt");
                        else
                            stack.Push(Math.Sqrt(value).ToString());
                    }
                    else
                        stack.Push(funcs_dict[token](Convert.ToDouble(stack.Pop())).ToString());
                }
                else if (token == "(")
                    throw new Exception("Wrong left parentheses in formula");
                ShowStack(stack);
            }
            if (stack.Count > 1)
                throw new Exception("Wrong number order");

            return Convert.ToDouble(stack.Pop());
        }

        public List<(double, double)> GetTabulatedValues(List<string> rpn, double x1, double x2, double step = 0.01)
        {
            List<(double, double)> result = new List<(double, double)>();

            for (; x1 <= x2 + 1e-12; x1 += step)
            {
                List<string> additional = new List<string>(rpn);

                for (int i = 0; i < additional.Count; i++)
                {
                    switch (additional[i])
                    {
                        case "x":
                            additional[i] = x1.ToString();
                            break;
                        case "PI":
                            additional[i] = "3,14";
                            break;
                        case "E":
                            additional[i] = "2,7";
                            break;
                        default:
                            break;
                    }
                }
                double yCoord = Calculate_RPN(additional);

                result.Add((Math.Round(x1, 5), Math.Round(yCoord, 5)));
            }

            return result;
        }
    }
}
