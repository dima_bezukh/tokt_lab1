﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1FrameWork
{
    public static class MathOperators
    {
        public static double Add(double value1, double value2) => value1 + value2;
        public static double Sub(double value1, double value2) => value1 - value2;
        public static double Mult(double value1, double value2) => value1 * value2;
        public static double Sin(double value) => Math.Sin(value);
        public static double Cos(double value) => Math.Cos(value);
        public static double Tg(double value) => Math.Tan(value);
        public static double Exp(double value) => Math.Exp(value);
        public static double Abs(double value) => Math.Abs(value);
    }
}
