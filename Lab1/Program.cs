﻿using System;
using System.Collections.Generic;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Algorithm algorithm = new Algorithm();
            Console.WriteLine(Math.Sin(9.11611117285042E+144));
            //Console.WriteLine(string.Join("; ", algorithm.Parse_To_RPN(algorithm.TokenizeString("1+sin(2,9*sin(3,14*2)/exp(tg(pow(4;5)+(pow(4;5)))-6*7*8))-9*tg(abs(10-cos(1+2,7))*sqrt(abs(tg(11+3,14)+tg(12))))"))));

            //Console.WriteLine(algorithm.Calculate_RPN(algorithm.Parse_To_RPN(algorithm.TokenizeString("1+sin(tg(2))*pow(2, 0)*0"))));

            var rpn = algorithm.Parse_To_RPN(algorithm.TokenizeString("1+sin(2,9*sin(E*x)/exp(tg(pow(4;5)+(pow(4;5)))-6*7*8))-9*tg(abs(10-cos(PI+E))*sqrt(abs(tg(11+PI)+tg(12))))"));

            //Console.WriteLine(string.Join("; ", algorithm.Calculate_RPN(rpn)));
            //algorithm.GetTabulatedValues(rpn, 0, 1);
            foreach (var i in algorithm.GetTabulatedValues(rpn, 0.01, 0.01))
            {
                Console.WriteLine($"{i.Item2}");
            }

        }
    }
}
