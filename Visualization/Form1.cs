﻿using Lab1FrameWork;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Visualization
{
    public partial class Form1 : Form
    {
        Algorithm library;
        public Form1()
        {
            library = new Algorithm();
            InitializeComponent();
        }

        private void calculate_Click(object sender, EventArgs e)
        {
            chart.Series[0].Points.Clear();
            string input = textBoxInput.Text;
            double startTab = Convert.ToDouble(x1.Text);
            double endTab = Convert.ToDouble(x2.Text);
            try
            {
                var rpn = library.Parse_To_RPN(library.TokenizeString(input));
                List<(double x, double y)> points = library.GetTabulatedValues(rpn, startTab, endTab);
                chart.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
                foreach (var point in points)
                    chart.Series[0].Points.AddXY(point.x, point.y);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chart_Click(object sender, EventArgs e)
        {

        }
    }
}
